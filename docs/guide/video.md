# CÓMO COLOCAR/CAMBIAR EL VIDEO INICIAL DE LA INTRANET DE FOGADE

## 👣 Pasos

1. Iniciar sesión en el administrador WordPress: 

`http://intranet.fogade.gob.ve/wp-admin`
   * Usuario: `Tromero`
   * Constraseña: `Thomasro_2903`

   ![inicio](./img/url.png)
2. Ir a Medios -> Añadir nuevo
    ![medios](./img/medios.png) 
    ![subir](./img/subirmedios.png)
3. Subir el archivo multimedia correspondiente
  ![seleccionar](./img/seleccionarvideo.png) 
4. Ir a Medios -> Biblioteca
5. Ubicar el archivo recién subido y hacer clic en `Editar` o en el nombre del archivo
6. Copiar la URL del archivo (aparece al lado derecho)
![editar](./img/copiarurl.png) 
7. Ir a Video PopUp -> On Page Load
![video](./img/videoPopUp.png) 
8. Pegar la URL en el campo `Video URL`
![pegar](./img/pegarurl.png) 
9. En `Display Options` seleccionar la opción **`Homepage only. You should to set the homepage settings from Reading Settings.`**

10. Finalmente hace clic en el botón `Guardar Cambios` (ubicado al final de la página)
![guardar](./img/guardar.png) 