export default {
  title: 'Intranet web ',
  description: 'sistema de red privado que permite compartir recursos entre sus miembros',
  base: '/', //  The default path during deployment / secondary address / base can be used/
  themeConfig: {
    logo: '/logo.png',
    nav: [
      { text: 'Inicio', link: '/' },      
      { text: 'Comenzar', link: '/guide/intro' },
      { text: 'Intranet', link: 'https://intranet-docs.gitlab.io/' },
      { text: 'GitHub', link: 'https://github.com/CaribesTIC/git-tuto' }      
    ],
    sidebar: [
      {
        text: 'Tutorial',   // required
        path: '/guide/',      // optional, link of the title, which should be an absolute path and must exist        
        sidebarDepth: 1,    // optional, defaults to 1 
        items: [
          { text: 'Introducción', link: '/guide/intro' },
          { text: 'Manejo de videos ', link: '/guide/video' },
        
        ]
      }
    ]
  }
}
